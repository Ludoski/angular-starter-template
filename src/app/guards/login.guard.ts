import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Injectable } from '@angular/core';

import { LocalStorageService } from '../services/local-storage.service';

// Guard checking if user is logged in
@Injectable({ providedIn: 'root' })
export class LoginGuard implements CanActivate {

  constructor(
    private router: Router,
    ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (LocalStorageService.accessToken && LocalStorageService.refreshToken) {
      this.router.navigate(['dashboard']);
    }
    return true;
  }
}
