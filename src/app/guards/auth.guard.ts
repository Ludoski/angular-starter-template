import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';

import { AuthService } from '../services/auth.service';
import { LocalStorageService } from '../services/local-storage.service';

// Guard checking if user is authenticated
@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {

  constructor(
    private router: Router,
    private authService: AuthService
    ) { }

  canActivate() {
    if (LocalStorageService.accessToken && LocalStorageService.refreshToken) {
      return true;
    }
    this.authService.logout();
    this.router.navigate(['login']);
    return false;
  }
}
