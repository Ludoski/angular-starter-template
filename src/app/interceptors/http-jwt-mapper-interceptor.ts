import { catchError, switchMap, filter, take } from 'rxjs/operators';
import { HTTP_INTERCEPTORS, HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError, BehaviorSubject, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment as env } from '../../environments/environment';

import { HttpConstant } from '../constants/http.constant';

import { AuthService } from '../services/auth.service';
import { TokensService } from '../services/tokens.service';
import { TokensModel } from '../models/tokens.model';
import { ResponseModel } from '../models/response.model';
import { Router } from '@angular/router';

@Injectable()
export class HttpJwtMapperInterceptor implements HttpInterceptor {

  private refreshingInProgress = false;
  private accessToken: string = '';
  private refreshToken: string = '';
  private accessTokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>('');

  constructor(
    private router: Router,
    private tokensService: TokensService,
    private authService: AuthService
    ) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.resolveTokens();

    return next.handle(this.addAuthorizationHeader(req, this.accessToken)).pipe(
      catchError(err => {
        // in case of 401 http error
        if (err instanceof HttpErrorResponse && err.status === 401) {
          // if 401 is from refresh token
          if (err.url === env.API_URL + HttpConstant.REFRESH_TOKEN_URL) {
            this.refreshingInProgress = false;
            this.logoutAndRedirect('Token refresh failed. Logging out.');
          }

          // try to refresh tokens
          return this.refreshTokens(req, next);
        }

        // if error is not 401 then just return this error
        return throwError(err);
      })
    );
  }

  private resolveTokens(): void {
    const potentialAccessToken: string | null = TokensService.accessToken;
    if (potentialAccessToken === null) {
      this.logoutAndRedirect('Invalid access token. Logging out.');
    } else {
      this.accessToken = potentialAccessToken;
    }
    const potentialRefreshToken: string | null = TokensService.refreshToken;
    if (potentialRefreshToken === null) {
      this.logoutAndRedirect('Invalid refresh token. Logging out.');
    } else {
      this.refreshToken = potentialRefreshToken;
    }
  }

  private addAuthorizationHeader(request: HttpRequest<any>, token: string): HttpRequest<any> {
    if (token && !this.refreshingInProgress) {
      return request.clone({setHeaders: {Authorization: `Bearer ${token}`}});
    }
    return request;
  }

  private logoutAndRedirect(err: string): Observable<HttpEvent<any>> {
    this.authService.logout();
    this.router.navigate(['login']);
      return throwError(() => new Error(err));
  }

  private refreshTokens(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!this.refreshingInProgress) {
      this.refreshingInProgress = true;
      this.accessTokenSubject.next('');
      return this.tokensService.refreshTokens(this.refreshToken).pipe(
        switchMap((response: ResponseModel) => {
          const tokens = response.message as TokensModel;
          if (tokens) {
            this.refreshingInProgress = false;
            this.accessToken = tokens.accessToken;
            // repeat failed request with new token
            return next.handle(this.addAuthorizationHeader(request, this.accessToken));
          }
          const errorMessage = 'Token refresh failed. Logging out.';
          this.logoutAndRedirect(errorMessage);
          return throwError(() => new Error(errorMessage));
        })
      );
    } else {
      // wait while getting new token
      return this.accessTokenSubject.pipe(
        filter(token => token !== null),
        take(1),
        switchMap(token => {
          // repeat failed request with new token
          return next.handle(this.addAuthorizationHeader(request, token));
        })
      );
    }
  }

}

export const HttpJwtMapperInterceptorProvider = {
  provide: HTTP_INTERCEPTORS,
  useClass: HttpJwtMapperInterceptor,
  multi: true
};