import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { SettingsConstant } from 'src/app/constants/settings.constant';
import { ResponseModel } from 'src/app/models/response.model';
import { UserModel } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';
import { NotificationsService } from 'src/app/services/notifications.service';
import { StoreService } from 'src/app/services/store.service';
import { UserProfileService } from 'src/app/services/user-profile.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  private pageTitle = 'Login';

  loginFG: FormGroup;
  currentUser: UserModel | undefined;

  constructor(
    private title: Title,
    private formBuilder: FormBuilder,
    private router: Router,
    private storeService: StoreService,
    private authService: AuthService,
    private userProfileService: UserProfileService,
    private notificationService: NotificationsService
  ) {
    this.title.setTitle(this.pageTitle + SettingsConstant.PAGE_TITLE_SUFFIX);
    // Account form group
    this.loginFG = this.formBuilder.group({
      email: [null, {validators: [Validators.required, Validators.email], updateOn: 'submit'}],
      password: [null, {validators: [Validators.required, Validators.minLength(1)], updateOn: 'submit'}]
    });
  }

  ngOnInit(): void {
    // Subscribe to logged user from store
    this.storeService.currentUser.subscribe(
      user => {
        this.currentUser = user;
      }
    )
  }

  // Form control getters
  get email(): FormControl { return this.loginFG.get('email') as FormControl; }
  get password(): FormControl { return this.loginFG.get('password') as FormControl; }

  // Form invalid methods
  emailInvalid(): boolean { return this.email.invalid && (this.email.dirty || this.email.touched); }
  passwordInvalid(): boolean { return this.password.invalid && (this.password.dirty || this.password.touched); }   

  // Submit credentials
  submit(): void {
    // Set form controls dirty
    this.email.markAsDirty();
    this.password.markAsDirty();

    // Return if form is invalid
    if (!this.loginFG.valid) return;

    // Send login request to backend
    this.authService.login(this.email.value, this.password.value).subscribe({
      error: error => {
        this.notificationService.showError(error.error.message);
        this.authService.logout();
      },
      complete: () => {
        // Get user entity from backend
        this.userProfileService.me().subscribe({
          next: (userResponse: ResponseModel) => {
            // Save user
            const user = userResponse.message as UserModel;
            this.storeService.saveCurrentUser(user);

            // Navigate to dashboard
            this.router.navigate(['dashboard']);
          },
          error: error => {
            this.notificationService.showError(error.error.message);
            this.authService.logout();
          }
        })
      }
    })
  }  

}
