import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { SettingsConstant } from 'src/app/constants/settings.constant';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  private pageTitle = 'Dashboard';

  constructor(
    private title: Title
  ) { }

  ngOnInit(): void {
    this.title.setTitle(this.pageTitle + SettingsConstant.PAGE_TITLE_SUFFIX);
  }

}
