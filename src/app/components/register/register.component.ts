import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { SettingsConstant } from 'src/app/constants/settings.constant';
import { MustMatch } from 'src/app/helpers/must-match.validator';
import { UserRegisterModel } from 'src/app/models/user-register.model';
import { UserModel } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';
import { NotificationsService } from 'src/app/services/notifications.service';
import { StoreService } from 'src/app/services/store.service';
import { UserProfileService } from 'src/app/services/user-profile.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  private pageTitle = 'Register';

  registerFG: FormGroup;
  currentUser: UserModel | undefined;

  constructor(
    private title: Title,
    private formBuilder: FormBuilder,
    private router: Router,
    private storeService: StoreService,
    private authService: AuthService,
    private userProfileService: UserProfileService,
    private notificationService: NotificationsService
  ) {
    this.title.setTitle(this.pageTitle + SettingsConstant.PAGE_TITLE_SUFFIX);
    // Account form group
    this.registerFG = this.formBuilder.group({
      email: [null, {validators: [Validators.required, Validators.email], updateOn: 'submit'}],
      password: [null, {validators: [Validators.required, Validators.minLength(8)], updateOn: 'submit'}],
      confirmedPassword: [null, {validators: [Validators.required, Validators.minLength(8)], updateOn: 'submit'}],
      username: [null, {validators: [Validators.required], updateOn: 'submit'}],
      firstName: [null, {validators: [Validators.required], updateOn: 'submit'}],
      lastName: [null, {validators: [Validators.required], updateOn: 'submit'}]
    },
    {validator: MustMatch('password', 'confirmedPassword')}
    );
  }

  ngOnInit(): void {
    this.title.setTitle(this.pageTitle + SettingsConstant.PAGE_TITLE_SUFFIX);
    
    // Subscribe to logged user from store
    this.storeService.currentUser.subscribe(
      user => {
        this.currentUser = user;
      }
    )
  }

  // Form control getters
  get email(): FormControl { return this.registerFG.get('email') as FormControl; }
  get password(): FormControl { return this.registerFG.get('password') as FormControl; }
  get confirmedPassword(): FormControl { return this.registerFG.get('confirmedPassword') as FormControl; }
  get username(): FormControl { return this.registerFG.get('username') as FormControl; }
  get firstName(): FormControl { return this.registerFG.get('firstName') as FormControl; }
  get lastName(): FormControl { return this.registerFG.get('lastName') as FormControl; }

  // Form invalid methods
  emailInvalid(): boolean { return this.email.invalid && (this.email.dirty || this.email.touched); }
  passwordInvalid(): boolean { return this.password.invalid && (this.password.dirty || this.password.touched); }
  confirmedPasswordInvalid(): boolean { return this.confirmedPassword.invalid && (this.confirmedPassword.dirty || this.confirmedPassword.touched); }
  usernameInvalid(): boolean { return this.username.invalid && (this.username.dirty || this.username.touched); }
  firstNameInvalid(): boolean { return this.firstName.invalid && (this.firstName.dirty || this.firstName.touched); }
  lastNameInvalid(): boolean { return this.lastName.invalid && (this.lastName.dirty || this.lastName.touched); }

  // Submit credentials
  submit(): void {
    // Set form controls dirty
    this.email.markAsDirty();
    this.password.markAsDirty();
    this.confirmedPassword.markAsDirty();
    this.username.markAsDirty();
    this.firstName.markAsDirty();
    this.lastName.markAsDirty();

    // Return if form is invalid
    if (!this.registerFG.valid) return;

    // Prepare user information
    const user: UserRegisterModel = {
      email: this.email.value,
      password: this.password.value,
      username: this.username.value,
      firstName: this.firstName.value,
      lastName: this.lastName.value
    }

    // Send register request to backend
    this.authService.register(user).subscribe({
      next: () => {
        this.notificationService.showSuccess('Registration successful! Please login.');
        // Navigate to login
        this.router.navigate(['login']);
      },
      error: error => {
        this.notificationService.showError(error.error.message);
      }
    })
  }

}
