import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment as env } from '../../environments/environment';

import { HttpConstant } from '../constants/http.constant';

import { UserModel } from '../models/user.model';
import { ResponseModel } from '../models/response.model';

@Injectable({ providedIn: 'root' })
export class UserProfileService {

  constructor(
    private httpClient: HttpClient
    ) { }

  me() {
    return this.httpClient.get<ResponseModel>(env.API_URL + HttpConstant.ME_URL);
  }

  saveMe(user: UserModel) {
    const payload = {
      email: user.email,
      username: user.username,
      firstName: user.firstName,
      lastName: user.lastName
    }
    return this.httpClient.patch<ResponseModel>(env.API_URL + HttpConstant.UPDATE_USER_URL.replace('{user_id}', user.id.toString()), payload);
  }

}
