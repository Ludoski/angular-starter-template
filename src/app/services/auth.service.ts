import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment as env } from '../../environments/environment';

import { HttpConstant } from '../constants/http.constant';

import { TokensModel } from '../models/tokens.model';
import { UserModel } from '../models/user.model';

import { TokensService } from '../services/tokens.service';
import { StoreService } from '../services/store.service';
import { LocalStorageService } from '../services/local-storage.service';
import { Observable, tap } from 'rxjs';
import { ResponseModel } from '../models/response.model';
import { UserRegisterModel } from '../models/user-register.model';

@Injectable({ providedIn: 'root' })
export class AuthService {

  constructor(
    private router: Router,
    private httpClient: HttpClient,
    private tokensService: TokensService,
    private storeService: StoreService,
    private localStorageService: LocalStorageService
  ) { }

  register(user: UserRegisterModel) {
    const payload = {
      email: user.email,
      password: user.password,
      username: user.username,
      firstName: user.firstName,
      lastName: user.lastName
    }
    return this.httpClient.post<ResponseModel>(env.API_URL + HttpConstant.REGISTER_URL, payload);
  }

  login(email: string, password: string): Observable<ResponseModel> {
    const payload = {
      email,
      password
    }
    return this.httpClient.post<ResponseModel>(env.API_URL + HttpConstant.LOGIN_URL, payload).pipe(
      tap((response: ResponseModel) => {
        const tokens = response.message as TokensModel;
        this.tokensService.saveTokens(tokens);
      })
    );
  }

  logout() {
    this.tokensService.deleteTokens();
    this.localStorageService.deleteCurrentUser();
    this.storeService.currentUserSubject.next(undefined);
  }

  changePassword(oldPassword: string, newPassword: UserModel): Observable<ResponseModel> {
    const payload = {
      old_password: oldPassword,
      new_password: newPassword
    }
    return this.httpClient.post<ResponseModel>(env.API_URL + HttpConstant.CHANGE_PASSWORD_URL, payload);
  }

}
