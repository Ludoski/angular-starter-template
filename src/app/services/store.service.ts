import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

import { UserModel } from '../models/user.model';

import { LocalStorageService } from './local-storage.service';

@Injectable({ providedIn: 'root' })
export class StoreService {

  currentUserSubject: BehaviorSubject<UserModel | undefined>;
  currentUser: Observable<UserModel | undefined>;

  constructor(
    private localStorageService: LocalStorageService
  ) {
    this.currentUserSubject = new BehaviorSubject<UserModel | undefined>(undefined);
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public saveCurrentUser(user: UserModel) {
    this.currentUserSubject.next(user);
    this.localStorageService.saveCurrentUser(user);
  }

}
