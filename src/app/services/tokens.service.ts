import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment as env } from '../../environments/environment';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { HttpConstant } from '../constants/http.constant';

import { TokensModel } from '../models/tokens.model';

import { LocalStorageService } from '../services/local-storage.service';
import { ResponseModel } from '../models/response.model';

@Injectable({ providedIn: 'root' })
export class TokensService {

  constructor(
    private httpClient: HttpClient,
    private localStorageService: LocalStorageService
  ) { }

  public static get accessToken(): string | null {
    return LocalStorageService.accessToken;
  }

  public static get refreshToken(): string | null {
    return LocalStorageService.refreshToken;
  }

  public saveTokens(tokens: TokensModel): void {
    this.localStorageService.saveTokens(tokens);
  }

  public deleteTokens() {
    this.localStorageService.deleteTokens();
  }

  public refreshTokens(refreshToken: string): Observable<ResponseModel> {
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: 'Bearer ' + refreshToken
      })
    };
    return this.httpClient.post<ResponseModel>(env.API_URL + HttpConstant.REFRESH_TOKEN_URL, {}, httpOptions).pipe(
      tap(response => {
        const tokens = response.message as TokensModel;
        this.saveTokens(tokens);
      })
    );
  }

  public tokenExpired(token: string): boolean {
    const expiry = (JSON.parse(atob(token.split('.')[1]))).exp;
    return (Math.floor((new Date).getTime() / 1000)) >= expiry;
  }

}