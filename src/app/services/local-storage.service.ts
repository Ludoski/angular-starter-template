import { Injectable } from '@angular/core';

import { LocalStorageConstant } from '../constants/local-storage.constant';

import { UserModel } from '../models/user.model';
import { TokensModel } from '../models/tokens.model';

@Injectable({ providedIn: 'root' })
export class LocalStorageService {

  // Tokens

  public saveTokens(tokens: TokensModel): void {
    if (tokens.accessToken && tokens.refreshToken) {
      localStorage.setItem(LocalStorageConstant.STORAGE_ACCESS_TOKEN_KEY, tokens.accessToken);
      localStorage.setItem(LocalStorageConstant.STORAGE_REFRESH_TOKEN_KEY, tokens.refreshToken);
    }
  }

  public static get accessToken(): string | null {
    return localStorage.getItem(LocalStorageConstant.STORAGE_ACCESS_TOKEN_KEY);
  }

  public static get refreshToken(): string | null {
    return localStorage.getItem(LocalStorageConstant.STORAGE_REFRESH_TOKEN_KEY);
  }

  public deleteTokens(): void {
    localStorage.removeItem(LocalStorageConstant.STORAGE_ACCESS_TOKEN_KEY);
    localStorage.removeItem(LocalStorageConstant.STORAGE_REFRESH_TOKEN_KEY);
  }

  // Current user

  public saveCurrentUser(currentUser: UserModel) {
    localStorage.setItem(LocalStorageConstant.STORAGE_CURRENT_USER_KEY, JSON.stringify(currentUser));
  }

  public static get currentUser(): UserModel | null {
    const user = localStorage.getItem(LocalStorageConstant.STORAGE_CURRENT_USER_KEY);
    return user != null ? JSON.parse(user) : null;
  }

  public deleteCurrentUser(): void {
    localStorage.removeItem(LocalStorageConstant.STORAGE_CURRENT_USER_KEY);
  }

}
