import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { SettingsConstant } from '../constants/settings.constant';

@Injectable({ providedIn: 'root' })
export class NotificationsService {

  private static readonly TIMEOUT: number = SettingsConstant.TIMEOUT;

  constructor(
    private toastr: ToastrService
  ) { }

  public showSuccess(message: string) {
    const title = "Success!";
    this.toastr.success(
      message,
      title,
      {
        timeOut: NotificationsService.TIMEOUT,
        closeButton: true,
        enableHtml: true,
        positionClass: 'toast-top-right'
      }
    );
  }

  public showInfo(message: string) {
    const title = "Info:";
    this.toastr.info(
      message,
      title,
      {
        timeOut: NotificationsService.TIMEOUT,
        closeButton: true,
        enableHtml: true,
        positionClass: 'toast-top-right'
      }
    );
  }

  public showWarning(message: string) {
    const title = "Warning:";
    this.toastr.warning(
      message,
      title,
      {
        timeOut: NotificationsService.TIMEOUT,
        closeButton: true,
        enableHtml: true,
        positionClass: 'toast-top-right'
      }
    );
  }

  public showError(message: string) {
    const title = "Error!";
    this.toastr.error(
      message,
      title,
      {
        timeOut: NotificationsService.TIMEOUT,
        closeButton: true,
        enableHtml: true,
        positionClass: 'toast-top-right'
      }
    );
  }

}
