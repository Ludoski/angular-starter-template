export enum HttpConstant {

  // Pagination
  ITEMS_PER_PAGE = 10,

  // Auth
  LOGIN_URL = '/auth/login',
  REGISTER_URL = '/auth/register',
  REFRESH_TOKEN_URL = '/auth/refresh',

  // User profile
  ME_URL = '/v1/users/me',
  UPDATE_USER_URL = '/users/{user_id}',
  CHANGE_PASSWORD_URL = '/auth/change_password'

}