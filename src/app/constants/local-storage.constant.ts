export enum LocalStorageConstant {
  STORAGE_ACCESS_TOKEN_KEY = 'access_token',
  STORAGE_REFRESH_TOKEN_KEY = 'refresh_token',
  STORAGE_CURRENT_USER_KEY = 'current_user',
}