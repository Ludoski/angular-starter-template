export enum SettingsConstant {

  // Misc
  PAGE_TITLE_SUFFIX = ' | Angular Template Starter',

  // Notifications
  TIMEOUT = 5000

}