import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { SettingsConstant } from './constants/settings.constant';
import { UserModel } from './models/user.model';
import { AuthService } from './services/auth.service';
import { LocalStorageService } from './services/local-storage.service';
import { StoreService } from './services/store.service';
import { TokensService } from './services/tokens.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  private pageTitle = 'App';

  currentUser: UserModel | undefined;

  constructor(
    private title: Title,
    private router: Router,
    private storeService: StoreService,
    private authService: AuthService
  ) {
    this.title.setTitle(this.pageTitle + SettingsConstant.PAGE_TITLE_SUFFIX);
  }

  ngOnInit(): void {
    // Subscribe to logged user from store
    this.storeService.currentUser.subscribe(
      user => {
        this.currentUser = user;
      }
    )

    // In case of page refresh
    if (
      TokensService.accessToken &&
      TokensService.refreshToken &&
      LocalStorageService.currentUser
      ) {
      this.storeService.currentUserSubject.next(LocalStorageService.currentUser);
    }
    else this.authService.logout();
  }
}
