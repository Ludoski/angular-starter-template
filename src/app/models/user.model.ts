export interface UserModel {
  id: string;
  email: string;
  username: string;
  role: string;
  active: boolean;
  firstName: string;
  lastName: string;
  createdAt: Date;
  lastLogin: Date;
  deactivatedAt: Date;
}